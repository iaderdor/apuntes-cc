\makeatletter
\def\input@path{{../}}
\makeatother

\documentclass[{../cc.tex}]{subfiles}
\begin{document}

\chapter{Cuantización del campo electromagnético}

En este capítulo vamos a aplicar el formalismo que vimos en el tema anterior al campo electromagnético.


\section{Cuantización de un sistema mecánico}

Vamos a comenzar a partir de un sistema mecánico con $N$ grados de libertad, en el cual conocemos $q_i, \dot{q}_i$. Sabemos que en estos sistemas la acción es la siguiente:

\begin{equation}
    S [q_i] = \int_{t_1} ^{t_2} \dd{t} \lag ( q_i, \dot{q}_i )
\end{equation}

Por el principio de mínima acción, una trayectoria debe tener la mínima acción posible, lo que equivale a lo siguiente:

\begin{equation}
  \delta S = \dv{t} \pdv{\lag}{\dot{q}_i} - \pdv{\lag}{q_i} = 0
\end{equation}

En donde hemos aprovechado para escribir las ecuaciones de Euler-Lagrange.

Por otro lado, también podemos usar el formalismo canónico de Hamilton, en el que partimos de los momentos canónicos conjugados, definidos como:

\begin{equation}
  \Pi_i = p_i = \pdv{\lag}{\dot{q}_i}
\end{equation}

Con la función hamiltoniana $\ham$ o hamiltoniano definido así:

\begin{equation}
  \ham ( q_i, p_i) = \sum_i p_i \dot{q}_i - \lag
\end{equation}

Estas ecuaciones nos reducen la EDO de orden dos de Euler-Lagrange a dos EDO de orden uno. Las ecuaciones de Hamilton son las siguientes:

\begin{equation}
  \dot{q}_i = \pdv{\ham}{p_i} \qquad%
  \dot{p}_o = - \pdv{\ham}{q_i}
\end{equation}


\subsection{Sistema cuántico}


Este formalismo se puede aplicar también a un sistema cuántico. En este caso, el hamiltoniano $\ham$ actúa sobre un espacio de estados $\ket{\psi(t)}$. Los observables se convierten en operadores, que representamos poniendo un gorrito encima de ellos, y que poseen el siguiente conmutador, equivalente a los corchetes de Poisson del formalismo canónico:

\begin{equation}
  \begin{cases}
    q_i &\rightarrow \hat{q}_i \\
    p_i &\rightarrow \hat{p}_i
  \end{cases}%
  \qquad \rightarrow \qquad%
  \comm{\hat{q}_i}{\hat{p}_i} = i \hbar \delta_{ij}
\end{equation}

La ecuación que da la evolución temporal del sistema es la ecuación de Schrödinger, que es la siguiente:

\begin{equation}
  i \hbar \pdv{ \ket{ \psi (t) } }{t} = \hat{ \ham } \ket{\psi (t) }
\end{equation}

Para un observable $\hat{A} (\hat{q}, \hat{p})$, hallamos el valor esperado del observable haciendo la siguiente operación:

\begin{equation}
  \ev{\hat{A}} = \ev{\hat{A}}{\psi}
\end{equation}

Por otro lado, para estados estacionarios en los que se cumpla la siguiente ecuación:

\begin{equation}
  \hat{H} \ket{\phi_n} = E_n \ket{\phi_n}
\end{equation}

Obtenemos que la evolución temporal del sistema es la siguiente:

\begin{equation}
  \ket{ \psi_n (t) } = e^{ -i E_n t} \ket{ \psi_n }
\end{equation}


\subsection{Oscilador armónico cuántico}

El oscilador armónico tiene la siguiente ecuación:

\begin{equation}
  \ham = \frac{ p^2 }{ 2m } + \frac{1}{2} m \omega^2 q^2
\end{equation}

Que, cuantizada, se escribe del siguiente modo:

\begin{equation}
  \hat{\ham} = \frac{ \hat{p}^2 }{ 2m } + \frac{1}{2} m \omega^2 \hat{q}^2
\end{equation}

Para resolver este hamiltoniano podemos usar dos métodos, el \textit{analítico} supone usar los operadores en la representación de Schrödinger:

\begin{equation}
  \begin{cases}
    \hat{q} &\rightarrow q \\
    \hat{p} &\rightarrow -i \pdv{q}
  \end{cases}
\end{equation}

Aplicando el hamiltoniano a la función de honda, veremos que la solución son polinomios de Hermite.

Podemos usar también la forma \textit{algebraica} para resolverla. En este método consideramos que los operadores $\hat{q}, \hat{p}$ son iguales a $\hat{q}^\dagger, \hat{p}^\dagger$, y vamos a definir los siguientes operadores, que no son autoadjuntos:

\begin{equation}
  \begin{cases}
    \hat{a} &= \sqrt{ \frac{ m \omega }{2} } \left( \hat{q} + \frac{i}{m} \hat{p} \right) \\
    \hat{a}^\dagger &=  \sqrt{ \frac{ m \omega }{2} } \left( \hat{q} - \frac{i}{m} \hat{p} \right)
  \end{cases}
\end{equation}

En donde $\hat{a}$ y $\hat{a}^\dagger$ son los operadores de destrucción y de cración. Una regla nmemotécnica para recordad cuál es cada uno es que el operador de destrución no tiene la daga porque la ha usado para destruir algo.

Los operadores $\hat{q}, \hat{p}$ entonces resultan ser los siguientes:

\begin{equation}
  \begin{cases}
    \hat{q} &= \frac{1}{ \sqrt{ 2 m \omega} } ( \hat{a}^\dagger + \hat{a} ) \\
    \hat{p} &= i \sqrt{ \frac{ m \omega}{2} } ( \hat{a}^\dagger - \hat{a} )
  \end{cases}
\end{equation}

En este caso, los operadores sí que son autoadjuntos. Además, nos interesa introducir el operador número, definido como:

\begin{equation}
  \hat{N} = \hat{a}^\dagger \hat{a}
\end{equation}

Aplicando estos operadores para reescribir el hamiltoniano, podemos resolver la ecuación. Vamos a calcular lo siguiente:

\begin{equation}
  \begin{split}
    \omega \hat{N} &=%
     \omega ( \hat{a}^\dagger \hat{a}) =%
    \frac{1}{2} m \omega^2 \left( \hat{q}^2 - \frac{i}{ m \omega } \hat{p} \hat{q} + \frac{i}{ m \omega } \hat{q} \hat{p} + \frac{ \hat{p}^2}{ m^2 \omega^2 } \right) \\
    &= \frac{1}{2} m \omega^2 \left( \hat{q}^2 - \frac{1}{ m \omega } + \frac{ \hat{p}^2}{ m^2 \omega^2 } \right) \\
    &= \frac{ \hat{p}^2 }{ 2m } + \frac{1}{2} m \omega^2 \hat{q}^2 - \frac{1}{2}
  \end{split}
\end{equation}

Felizmente, hemos hallado el hamiltoniano, con lo cual, podemos reescribirlo de la siguiente manera:

\begin{framed}
  \begin{equation}
    \hat{\ham} = \omega ( \hat{N} + \frac{1}{2} )
  \end{equation}
\end{framed}

Analicemos ahora el operador número. Este operador tiene las siguientes propiedades:

\begin{equation}
  \hat{N} = \hat{N}^\dagger \quad \rightarrow \quad%
  \hat{N} \ket{\mu} = \mu \ket{\mu} \quad : \quad \mu \in \mathbb{R}
\end{equation}

Antes de nada, vamos a suponer que existe un $\ket{\mu}$ tal que:

\begin{equation}
  \hat{a} \ket{\mu} = 0
\end{equation}

Con lo cual, $\hat{N} \ket{\mu} = 0$. Por otro lado, actúa del siguiente modo:

\begin{equation}
  \begin{cases}
    \hat{N} \ket{\mu} &= 0 \\
    \hat{N} \ket{\mu} &= \mu \ket{\mu}
  \end{cases}
\end{equation}

Visto esto, definimos $\ket{\mu} = 0$, como $\hat{a}\ket{0} = 0$. Por otro lado, podemos hacer lo siguiente, para ver cómo actúa $\hat{a}^\dagger$:

\begin{equation}
  \begin{split}
    \hat{a}^\dagger \ket{\mu} \\
    \norm{ \hat{a}^\dagger }^2 &= \ev{\hat{a}\hat{a}^\dagger}{\mu} \\
    &= \ev{ \comm{\hat{a}}{\hat{a}^\dagger} + \hat{a}^\dagger \hat{a}  }{\mu} \\
    &= \innerproduct{\mu} + \mu \innerproduct{\mu} \\
    &= \mu + 1
  \end{split}
\end{equation}

Con esto, ya tenemos el significado y podemos aplicar el hamiltoniano.

\begin{equation}
  \hat{\hat} \ket{0} = \omega ( \hat{N} + \frac{1}{2}) \ket{0} = \frac{\omega}{2} \ket{0}
\end{equation}

Por otro lado, tenemos las siguientes relaciones:

\begin{equation}
  \begin{cases}
    \hat{N} \hat{A} \ket{\mu} &= ( -1 + \mu ) \ket{\mu} \\
    \hat{N} \hat{A} \ket{\mu} &= ( \mu + 1 ) \ket{\mu}
  \end{cases}
\end{equation}

Podemos ahora construir todo el espectro del hamiltoniano con $\hat{a}$ y $\hat{a}^\dagger$. Estos operadores nos "crean" partículas en un estado, de tal modo que para hallar $\ket{n}$ podemos aplicar $n$ veces $\hat{a}^\dagger$ con la correspondiente constante de normalización:

\begin{equation}
  \ket{n} = \frac{1}{\sqrt{n}} ( \hat{a}^\dagger )^n \ket{0}
\end{equation}

Así pues, el operador número actuará así:

\begin{equation}
  \hat{N} \ket{n} = n \ket{n}
\end{equation}

Con lo cual, el espectro de energías del hamiltoniano, es el siguiente:

\begin{equation}
  E_n = \omega( n + \frac{1}{2})
\end{equation}

En el que hemos tomado ya que $\hbar=1$. Con lo cual, tenemos el siguiente hamiltoniano:

\begin{equation}
  \hat{\ham} = \omega ( \hat{N} + \frac{1}{2}) \qquad%
  \hat{\ham} \ket{n} = \omega ( n + \frac{1}{2}) \ket{n}
\end{equation}

De tal modo que los operadores de creación y de destrucción actúan de la siguiente manera:

\begin{equation}
  \begin{cases}
    \hat{a}^\dagger \ket{n} &= \sqrt{ n+1 } \ket{ n + 1 } \\
    \hat{a} \ket{n} &= \sqrt{n} \ket{ n-1 }
  \end{cases}
\end{equation}

una nota importante es que la diferencia de energías no depende de dónde estén los estados. Cada estado es equiespaciado:

\begin{equation}
  E_{ n+1 } - E_n = \omega \qquad : \qquad \forall n
\end{equation}

El oscilador armónico podemos interpretarlo como un sistema en el que introducimos $n$ objetos de energía $\omega$. Al meter en la caja $n$ objetos, tenemos el equivalente a tener un estado $\ket{n}$ del oscilador.

Según esta interpretación, tenemos:

\begin{itemize}
  \item $\hat{a}^\dagger$ añade una partícula al sistema
  \item $\hat{a}$ elimina una partícula del sistema.
\end{itemize}



\section{Repaso del campo electromagnético clásico}


Damos por hecho que vamos a usar unidades naturales y racionalizado. Nos interesa hacer las ecuaciones covariantes, así que en primer lugar vamos a definir dos cuadrivectores:

\begin{equation}
  \begin{cases}
    J^\mu &=( \rho , \vec{j} ) \qquad \text{4-vector corriente}\\
    A^\mu &= ( \phi, \vec{A} ) \qquad \text{4-vector potencial}
  \end{cases}
\end{equation}

A partir de $\vec{A}$ se pueden hallar los campos electromagnéticos. Como tal,los potenciales no se observan, ya que los responsables de que las cargas se muevan son los campos en sí. Recordemos que los potenciales se definen a partir de los campos del siguiente modo:

\begin{equation}
  \vec{E} = -\nabla \phi - \pdv{ \vec{A} }{t} \qquad%
  \vec{B} = \curl \vec{A}
\end{equation}

Se puede hacer una formulación covariante del campo electromangético. Existe el tensor electromagnético $F_{\mu \nu}$, que es el siguiente:

\begin{equation}
  F_{\mu \nu} = \partial_\mu A_\nu - \partial_\nu A_\mu =%
  \begin{pmatrix}
    0 & E_x & E_y & E_z \\
    E_x & 0 & -B_z & B_y \\
    E_y & B_z & 0 & - B_x \\
    -E_z & -B_y & B_x & 0
  \end{pmatrix}
\end{equation}

Dicho de otro modo, podemos extraer las componentes del campo eléctrico y magnético del siguiente modo:

\begin{equation}
  E^i = F_{ 0i } \qquad%
  B^i = - \frac{1}{2} \epsilon_{ijk} F_{jk}
\end{equation}

Ahora vamos a trabajar con las ecuaciones de Maxwell, que son las siguientes:

\begin{equation}
  \begin{split}
    \div{ \vec{B} } = 0  &\qquad \div{ \vec{E}} = \rho \\
    \curl{ \vec{E} } = - \pdv{ \vec{B}}{t} &\qquad \curl{ \vec{B}} = \vec{j} + \pdv{\vec{E}}{t}
  \end{split}
\end{equation}

A partir de estas ecuaciones, puede escribirse la siguiente ecuación, que no es más que la ecuación de Maxwell en forma covariante:

\begin{equation}
  \partial_\mu F^{ \mu \nu } = F^\nu
\end{equation}


\subsection{Invariancia gauge}

La invariancia gauge es importante en la física. El electromagnetismo tiene una invariancia gauge, ya que los distintos potenciales no son unívocos. Varios potenciales pueden dar los mismos campos.

Comprobemos esto, definiento la siguiente transformación del potencial:

\begin{equation}
  A^\mu \quad\rightarrow \quad%
  A'^\mu = A^\mu + \partial^\mu \phi \qquad : \qquad \phi \text{ escalar}
\end{equation}

Apliquémoslo a los potenciales por separado:

\begin{equation}
  A^\mu =%
  \begin{cases}
    \phi \quad \rightarrow \quad \phi' &= \phi - \partial_t \phi \\
    \vec{A} \quad \rightarrow \quad \vec{A}' &= \vec{A} - \nabla \phi
  \end{cases}
\end{equation}

Ahora vamos a demostrar que, bajo este cambio, los campos son los mismos:

\begin{equation}
  \begin{split}
    F'^{ \mu \nu } &= \partial^\mu A^\nu - \partial^\nu A^\mu =%
    \partial^\mu ( A^\nu + \partial^\nu \phi) - \partial^\nu ( A^\mu + \partial^\mu \phi ) \\
    &= \partial^\mu A^\nu + \partial^\mu \partial^\nu \phi - \partial^\nu A^\mu - \partial^\nu \partial^\mu \phi \\
    &=\partial^\mu A^\nu - \partial^\nu A^\mu \\
    &= F^{ \mu \nu }
  \end{split}
\end{equation}

Con esto acabamos de ver en esencia lo que es una transformación de gauge.

Ahora vamos a expresar la cuadricorriente en función del cuadripotencial. Usando directamente la definición de $F_{ \mu \nu}$:

\begin{equation}
  \partial_\mu F^{ \mu \nu } = \partial_\mu ( \partial^\mu A^\nu - \partial ^\nu A^\mu) = J^\nu
\end{equation}

O, escrito del mismo modo, las ecuaciones de Maxwell en términos del potencial queda del siguiente modo:

\begin{framed}
\begin{equation}
  \Box A^\nu - \partial_\mu \partial^\nu A^\mu = J^\nu
\end{equation}
\end{framed}

Ahora, vamos a escoger $\phi$ de tal manera que cumpla que:

\begin{equation}
  \Box \phi = - \partial_\mu A^\mu.
\end{equation}

Este es el llamado gauge de Lorenz (no de Lorentz). Vamos a comprobar que los potenciales se quedan igual. Tenemos una transformación como la siguiente:

\begin{equation}
  A'^\mu = A^\mu + \partial^\mu \phi
\end{equation}

Con lo cual:

\begin{equation}
  \partial_\mu A'^\mu = \partial_\mu a^\mu \partial_\mu \partial^\mu \phi = \partial_\mu A^\mu + \Box \phi = \partial_\mu A^\mu - \partial_\mu A^\mu = 0
\end{equation}

Escogiendo astutamente $\phi$, obtenemos ecuaciones distintas a las de antes. Contretamente, acabamos de hallar la \textbf{condición de Lorenz}, que es la siguiente:

\begin{equation}
  \partial_\mu A^\mu = 0
\end{equation}

Así pues, en este gauge, las ecuaciones de Lorentz se quedan del siguiente modo:

\begin{equation}
  \begin{split}
    \Box A^\nu - \partial\nu \cancelto{0}{\partial_\mu A^\mu} = J^\nu \\
    \Box A^\nu = J^\nu
  \end{split}
\end{equation}

Estas ecuaciones se pueden escribirdirectamente de la siguiente manera:

\begin{equation}
  \begin{cases}
    \partial_t^2 \phi - \nabla^2 \phi &= \rho \\
    \partial_t^2 \vec{A} - \nabla^2 \vec{A} &= \vec{j}
  \end{cases}
\end{equation}

Pero podemos ir más lejos y considerar el caso del vacío. En el vacío, $J^\mu = 0$. Las ecuaciones resultan entonces las siguientes:

\begin{equation}
  \Box A^\mu = 0 \qquad \rightarrow \qquad%
  \partial_t^2 A^\mu - \nabla^2 A^\mu = 0
\end{equation}

Por otro lado, hay otra tranformación de $\phi$, que nos va a servir para analizar cómo se propagan las ondas electromagnéticas. Vamos a tomar la siguiente:

\begin{equation}
  \phi \quad : \quad \pdv{\phi}{t} = -\phi
\end{equation}

Esto implica entonces lo siguiente:

\begin{equation}
  \begin{split}
    \phi' &= \phi + \partial_t \phi = \phi - \phi = 0 \\
    \vec{A}' &= \vec{A} - \nabla \phi
  \end{split}
\end{equation}

Y entonces, el potencial se transforma del siguiente modo:

\begin{equation}
  \begin{split}
    \nabla \vec{A}' &= \nabla \vec{A} - \nabla^2 \phi \\
    &= \nabla \vec{A} - \int \dd{t} \nabla^2 \phi \\
    &= \nabla \vec{A} - \int \dd{t} \partial_t^2 \phi \\
    &= \nabla \vec{A} - \partial_t \phi \\
    &= - \partial_\mu A^\mu \\
    &= 0
  \end{split}
\end{equation}

Con esto, tenemos que $\nabla \vec{A}' = 0 \phi'$. Así pues, si tenemos que $\phi = 0$, $\nabla \vec{A} = 0$, tenemos las ecuaciones de Maxwell en el vacío, que van a ser lo siguiente:

\begin{framed}
\begin{equation}
  \Box \vec{A} = 0
\end{equation}
\end{framed}

Este es el llamado gauge de Coulomb o transverlas. Hay que tener cuidado, ya que esta ecuación no es covariante, ya que es un D'Alambertiano aplicado sobre un trivector.

La ecuación que hemos obtenido no es otra que la ecuación de onda:

\begin{equation}
  \Box \vec{A} = \pdv[2]{ \vec{A} }{t} - \nabla^2 \vec{A} = 0 \qquad%
  \begin{cases}
    \Box \vec{B} &= 0 \\
    \Box \vec{E} &= 0
  \end{cases}
\end{equation}

Busquemos soluciones a dicha ecuación. Propongamos $u$:

\begin{equation}
  u = \epsilon_\mu ( \vec{k}, \lambda) e^{ -i k_\mu x^\mu}
\end{equation}

Apliquemosla ahora al D'Alambertiano:

\begin{equation}
  \Box ( \epsilon_\mu ( \vec{k}, \lambda) e^{ -i k_\mu x^\mu} ) =%
  - k^2 \epsilon_\mu e^{ -i k_\mu x^\mu } = 0
\end{equation}

Para que se cumpla la ecuación, tenemos que tener $k^2 = 0$. $k^\mu$ es un 4-vector, con lo cual, tiene que cumplirse:

\begin{equation}
  k^{0 2} - \vec{k}^2 = 0 \qquad k^0 = \abs{ \vec{k} }
\end{equation}

Físicamente, $k^0$ es la frecuencia, mientras que $\vec{k}$ es el vector de onda.

$\lambda$ y $\vec{k}$ son los vectores de polarización. Puesto que tenemos que $\partial_\mu A^\mu = 0$, así que:

\begin{equation}
  k_\mu \epsilon^\mu = 0 \qquad%
  k_0 \epsilon^0 - \vec{k} \vec{E} = 0
\end{equation}

Pero con el gauge de Coulomb, tenemos:

\begin{equation}
  \phi = 0 = A^0 \qquad , \qquad%
  \div{ \vec{A} } = 0 \quad \leadsto \quad%
  \vec{k} \cdot \vec{A} = 0
\end{equation}

Inicialmente, teníamos $\epsilon_\mu ( \vec{k}, \lambda )$, pero ahora tenemos que $\epsilon_\mu ( 0, \vec{E} ( k, \lambda ))$, pero sólo puede tomar 2 valores posibles, ya que ha de ser perpendicular:

\begin{equation}
  \vec{\epsilon} ( \vec{k}, \lambda ) \cdot \vec{k} = 0 \quad%
  \begin{cases}
    \vec{\epsilon}_1 &= \vec{\epsilon} ( \vec{k}, 1 ) \\
    \vec{\epsilon}_2 &= \vec{\epsilon} ( \vec{k}, 2) \\
    \vec{\epsilon}_3 &= \frac{ \vec{k} }{\omega}
  \end{cases}
\end{equation}

En donde además se cumple que $ \vec{\epsilon_3} = \vec{\epsilon_1} \cross \vec{\epsilon_2}$. Para cada $\vec{k}$, tenemos 2 estados de polarización que son perpendiculares. Más generalmente, podemos construir lo siguiente:

\begin{equation}
  \begin{cases}
    \epsilon_3 &= \left( \sin \theta \cos \phi , \sin \theta \cos \phi, \cos \theta \right) \\
    \epsilon_1 &= \left( \cos \theta \cos \phi, \cos \theta \sin \phi, -\sin \theta \right) \\
    \epsilon_2 &= \left( - \sin \phi, \cos \phi, 0 \right)
  \end{cases}
\end{equation}

Puede verse con esto que las soluciones que hemos obtenido son indas electromagnéticas. $\vec{A}$ siempres e mueve en el plano transverso, y nos da los distintos tipos de polarización de la luz.

Podemos expresar también la polarización circular del siguiente modo:

\begin{equation}
  \vec{\epsilon}_{ \pm } = \mp \frac{1}{\sqrt{2}} ( \vec{\epsilon}_1 \pm i \vec{\epsilon}_2)
\end{equation}




\end{document}
